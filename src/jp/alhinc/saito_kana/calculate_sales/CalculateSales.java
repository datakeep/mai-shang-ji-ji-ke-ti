package jp.alhinc.saito_kana.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales{
	public static void main (String[] args) {

		// コマンドライン引数が渡されていない場合のエラー処理
		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		Map<String, String> branchNames = new HashMap<>();
		Map<String, Long> branchSales = new HashMap<>();

		if(readFile(args[0], branchNames, branchSales, "branch.lst", "支店", "^[0-9]{3}") == false){
			return;
		}
		Map<String, String> productNames = new HashMap<>();
		Map<String, Long> productSales = new HashMap<>();

		if(readFile(args[0], productNames, productSales, "commodity.lst", "商品", "^[0-9A-Z]{8}") == false){
			return;
		}

		// 売上ファイルの読み込み処理
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		for(int i = 0; i < files.length ; i++) {
			String fileName = files[i].getName();
			if(files[i].isFile() && fileName.matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}
		Collections.sort(rcdFiles);

		// 売上ファイルが連番になっていない場合のエラー処理
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0,8));
			if((latter  - former) != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		BufferedReader br = null;

		for(int i = 0; i< rcdFiles.size(); i++){
			try{
				br = new BufferedReader(new FileReader(rcdFiles.get(i)));

				ArrayList<String> fileContents = new ArrayList<>();
				String line = "";
				while((line = br.readLine()) != null){
					fileContents.add(line);
				}
				String fileName = rcdFiles.get(i).getName();

				// 行数チェック
				if(fileContents.size() != 3) {
					System.out.println(fileName + "のフォーマットが不正です");
					return;
				}

				// 売上ファイルの売上金額が数字ではない場合のエラー処理
				if(!fileContents.get(2).matches("^[0-9]+$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				//支店コード支店定義ファイルに該当しなかった場合
				if(!branchNames.containsKey(fileContents.get(0))) {
					System.out.println(fileName + "の支店コードが不正です");
					return;
				}

				//商品コード支店定義ファイルに該当しなかった場合
				if(!productNames.containsKey(fileContents.get(1))) {
					System.out.println(fileName + "の商品コードが不正です");
					return;
				}

				String branchCode = fileContents.get(0);
				String productCode = fileContents.get(1);

				// 支店コードの紐づけで売り上げ金額の加算
				long fileSale = Long.parseLong(fileContents.get(2));
				Long saleAmount = branchSales.get(branchCode) + fileSale;

				//商品コードの紐づけで売上金額の加算
				Long saleProductAmount = productSales.get(productCode) + fileSale;

				// 集計した売上金額が10桁を超えた場合のエラー処理
				if(saleAmount >= 10000000000L ) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				if(saleProductAmount >= 10000000000L ) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				// branchSalesマップに支店コードをキーに売上額を格納
				branchSales.put(branchCode, saleAmount);

				// productSalesマップに商品コードをキーに売上額を格納
				productSales.put(productCode, saleProductAmount);

			}catch(IOException e) {
				System.out.println("エラーが発生しました。");
				return;
			}finally {
				if(br != null) {
					try {
						br.close();
					}catch(IOException e ) {
						System.out.println("closeできませんでした。");
						return;
					}
				}
			}
		}
		if(writeFile(args[0], branchNames, branchSales, "branch.out", "支店", "^[0-9]{3}") == false) {
			return;
		}
		if(writeFile(args[0], productNames, productSales, "commodity.out", "商品", "^[0-9A-Z]{8}") == false) {
			return;
		}
	}

	private static boolean readFile(String path, Map <String,String>names, Map <String,Long>sales, String listName,
			String name, String type) {

		// 支店定義、商品定義ファイル読み込み処理
		BufferedReader br = null;

		try {
			File file = new File(path, listName);
			FileReader fr = new FileReader(file);

			//定義ファイルがない場合のエラー処理
			if(!file.exists()) {
				System.out.println(name + "定義ファイルが存在しません");
				return false;
			}

			br = new BufferedReader(fr);

			String line;
			while((line = br.readLine()) != null){
				String[] items = line.split(",");

				/* 支店定義ファイルのフォーマットが違う場合のエラー処理
				if((items.length != 2) || (!items[0].matches("^[0-9]{3}$"))) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}
				// 商品定義ファイルのフォーマットが違う場合のエラー処理
				if((items.length != 2) || (!items[0].matches("^[SFT0-9]{8}$"))) {
					System.out.println("商品定義ファイルのフォーマットが不正です");
					return false;
				}上記をひとつにまとめる*/

				if((items.length != 2) || (!items[0].matches(type))) {
					System.out.println(name + "定義ファイルのフォーマットが不正です");
					return false;
				}

				names.put(items[0], items[1]);
				sales.put(items[0], 0L);

			}
		}catch(IOException e) {
			System.out.println("エラーが発生しました。");
			return false;
		}finally {
			if(br != null) {
				try {
					br.close();
				}catch(IOException e ) {
					System.out.println("closeできませんでした。");
					return false;
				}
			}
		}return true;
	}

	private static boolean writeFile(String path, Map<String,String> names, Map<String, Long> sales,
			String listName,String name, String type) {

		// 支店別集計ファイルの書き込み処理

		BufferedWriter bw = null;
		try {
			File file = new File(path, listName);
			bw = new BufferedWriter(new FileWriter(file));

			for(String key : names.keySet()) {
				bw.write(key + "," + names.get(key) + "," + sales.get(key));
				// 改行
				bw.newLine();
			}
		}catch(IOException e) {
			System.out.println("エラーが発生しました。");
			return false;
		}finally {
			if(bw != null) {
				try {
					bw.close();
				}catch(IOException e ) {
					System.out.println("closeできませんでした。");
					return false;
				}

			}
		}
		return true;
	}
}